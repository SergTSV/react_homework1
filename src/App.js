import React, { Component } from 'react';
import Button from './Components/Button/Button'
import './App.scss';
import Modal from './Components/Modal/Modal'

class App extends Component {
  state = {
    firstModal: false,
    secondModal: false,
  }

  showFirstModal = () => {
    this.setState({ firstModal: true });
  }
  showSecondModal = () => {
    this.setState({ secondModal: true });
  }

  closeModal = (event) => {
    this.setState({ firstModal: false })
    this.setState({ secondModal: false })
  }

  render() {
    return (
      <div className="App">
        <Button className="buttons" text={"Open first modal"} backgroundColor={"teal"}
          onClick={() => this.showFirstModal()} />
        <Button className="buttons" text={"Open second modal"} backgroundColor={"green"}
          onClick={() => this.showSecondModal()} />
        {this.state.firstModal ?
          <Modal backgroundColor='#f80303' color='#f8f6f6' header={"Do you want to delete this file?"} text={`Once you delete this file, it won't be possible to undo this action.
          Are you sure you want to delete it?`} closeButton={true} actions={
            <>
              <button className={'modal__buttons-btn'} onClick={() => this.closeModal()}>Ok</button>
              <button className={'modal__buttons-btn'} onClick={() => this.closeModal()}>Cancel</button>
            </>
          } onClick={(event) => this.closeModal(event)} /> : ''}
        {this.state.secondModal ?
          <Modal backgroundColor='#246dcc' color='#f8f6f6' header={"Second Modal"} text={"Are you ready?"} closeButton={false} actions={
            <>
              <button className={'modal__buttons-btn'} onClick={() => this.closeModal()}>Apply</button>
              <button className={'modal__buttons-btn'} onClick={() => this.closeModal()}> Cancel</button>
            </>
          } onClick={(event) => event.currentTarget === event.target && this.closeModal()} /> : ''}
      </div>
    )
  }
}

export default App;
